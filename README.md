## Install
- Download package `yarn add nuxt-iransans` or `npm install nuxt-iransans`

## Setup
- Add `"@nuxt-iransans"` to `buildModules` section of `nuxt.config.js`
```json
{
  "buildModules": ["nuxt-iransans"]
}
````

## Config
### font(String): defualt (IRANSansWeb(FaNum))
```json
[
  ["nuxt-iransans", { "font": "IRANSansWeb" }]
]
````
