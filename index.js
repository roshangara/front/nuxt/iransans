export default function SimpleModule(moduleOptions) {
    this.options.css.push(`roshangara-iransans/src/${moduleOptions.font ? moduleOptions.font : 'IRANSansWeb(FaNum)'}.css`)
}
module.exports.meta = require('./package.json');
